import { Component, OnInit } from '@angular/core';
import { ContratoService } from 'src/app/_services/contrato.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService, AlertService } from 'src/app/_services';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-contrato',
  templateUrl: './contrato.component.html',
  styleUrls: ['./contrato.component.less']
})
export class ContratoComponent implements OnInit {
  currentUserTCGED: User;
  points = [];
  forn = [];
  signatureImage1;
  signatureImage2;
  campos: any = [];
  contrato: any = [];

  formContrato = new FormGroup({
  });

  recibo = false;
  assinado1 = false;
  assinado2 = false;
  loading = false;
  finalizado = false;
  pdf;
  id: string;

  showImage1(data) {
    this.signatureImage1 = data;
    this.assinado1 = true;
  }

  showImage2(data) {
    this.signatureImage2 = data;
    this.assinado2 = true;
  }

  constructor(
    private contratoService: ContratoService,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
    this.currentUserTCGED = this.authenticationService.currentUserTCGEDValue;
  }

  submitForm() {
    this.alertService.clear();
    this.pdf = '';
    const formData = new FormData();
    formData.append('idUser', this.currentUserTCGED.id.toString());
    formData.append('nomeCliente', this.formContrato.get('nomeCliente').value);
    formData.append('docCliente', this.formContrato.get('docCliente').value);
    formData.append('endCliente', this.formContrato.get('endCliente').value);
    formData.append('qtdUnidades', this.formContrato.get('qtdUnidades').value);
    formData.append('nomeSignatario', this.formContrato.get('nomeSignatario').value);
    formData.append('cpfSignatario', this.formContrato.get('cpfSignatario').value);
    formData.append('nomeTestemunha', this.formContrato.get('nomeTestemunha').value);
    formData.append('cpfTestemunha', this.formContrato.get('cpfTestemunha').value);
    this.contratoService.getContrato(formData)
      .subscribe(f => {
        console.log(f)
        this.contrato = f.msg;
      },
        (err) => this.alertService.error(err.error.message)
      );
    this.recibo = true;
  }

  submitAssinatura() {
    this.loading = true;
    this.alertService.clear();
    const formData = new FormData();
    formData.append('idUser', this.currentUserTCGED.id.toString());
    formData.append('idContrato', this.contrato.id);
    formData.append('signature01', this.signatureImage1);
    formData.append('signature02', this.signatureImage2);
    this.contratoService.salvarContrato(formData).subscribe(
      (res) => {
        console.log(res);
        this.alertService.success(res.message);
        this.loading = false;
        this.formContrato.reset();
        this.pdf = res.msg.pdf;
        this.recibo = false;
        this.finalizado = true;
        this.assinado1 = false;
        this.assinado2 = false;
      },
      (err) => {
        console.log(err);
        this.alertService.error(err.error.message);
        this.loading = false;
      })
  }

  ngOnInit() {
    this.campos = JSON.parse(localStorage.getItem('currentUserTCGEDFields'));
    this.campos.forEach(element => {
      this.formContrato.addControl(element.nomeCampo, new FormControl('', Validators.required));
    });
  }

}
