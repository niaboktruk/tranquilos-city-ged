import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratopendenteComponent } from './contratopendente.component';

describe('ContratopendenteComponent', () => {
  let component: ContratopendenteComponent;
  let fixture: ComponentFixture<ContratopendenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratopendenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratopendenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
