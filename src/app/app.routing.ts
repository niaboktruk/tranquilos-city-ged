﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { ContratoComponent } from './contrato/contrato/contrato.component';
import { ContratopendenteComponent } from './contrato/contratopendente/contratopendente.component';
import { AssinaturaComponent } from './assinatura/assinatura.component';
import { DocumentoComponent } from './contrato/anexos/documento.component';
import { ExtratoComponent } from './recibo/extrato/extrato.component';
import { RecibocompraComponent } from './recibo/recibocompra/recibocompra.component';
import { RecibosaqueComponent } from './recibo/recibosaque/recibosaque.component';
import { PesquisafornComponent } from './pesquisaforn/pesquisaforn.component';
import { AuthGuard } from './_helpers/auth.guard';
import { RfidComponent } from './rfid/rfid.component';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'pesquisaforn/:rota', component: PesquisafornComponent, canActivate: [AuthGuard] },
    { path: 'contrato', component: ContratoComponent, canActivate: [AuthGuard]  },
    { path: 'contratopendente', component: ContratopendenteComponent, canActivate: [AuthGuard]  },
    { path: 'assinatura', component: AssinaturaComponent, canActivate: [AuthGuard]  },
    { path: 'assinatura/:contrato', component: AssinaturaComponent, canActivate: [AuthGuard]  },
    { path: 'anexo', component: DocumentoComponent, canActivate: [AuthGuard]  },
    { path: 'anexo/:contrato', component: DocumentoComponent, canActivate: [AuthGuard]  },
    { path: 'extrato/:id', component: ExtratoComponent, canActivate: [AuthGuard]  },
    { path: 'recibocompra/:id', component: RecibocompraComponent, canActivate: [AuthGuard]  },
    { path: 'recibosaque/:id', component: RecibosaqueComponent, canActivate: [AuthGuard]  },
    { path: 'rfid', component: RfidComponent, canActivate: [AuthGuard]  },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
