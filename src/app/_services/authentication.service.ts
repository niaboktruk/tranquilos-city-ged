﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserTCGEDSubject: BehaviorSubject<any>;
    public currentUserTCGED: Observable<any>;

    constructor(private http: HttpClient) {
        this.currentUserTCGEDSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUserTCGED')));
        this.currentUserTCGED = this.currentUserTCGEDSubject.asObservable();
    }

    public get currentUserTCGEDValue() {
        return this.currentUserTCGEDSubject.value;
    }

    login(username, password) {
        const formData = new FormData();
        formData.append('login', username);
        formData.append('password', password);
        return this.http.post<any>(`${environment.apiUrl}/users`, formData)
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUserTCGED', JSON.stringify(user.msg.data));
                localStorage.setItem('currentUserTCGEDFields', JSON.stringify(user.msg.fields));
                this.currentUserTCGEDSubject.next(user.msg.data);
                return user.msg.data;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUserTCGED');
        this.currentUserTCGEDSubject.next(null);
    }
}

