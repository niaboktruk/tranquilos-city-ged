﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ContratoService {
  constructor(private http: HttpClient) { }

  getContrato(formData: FormData): Observable<any> {
    return this.http.post(`${environment.apiUrl}/contrato`, formData);
  }

  salvarContrato(formData: FormData): Observable<any> {
    return this.http.post(`${environment.apiUrl}/contrato/add`, formData);
  }

}
