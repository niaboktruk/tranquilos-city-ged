﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    perfil(id: number) {
        return this.http.get(`${environment.apiUrl}/profile/${id}`);
    }

    salvarPerfil(formData: FormData) {
        return this.http.post(`${environment.apiUrl}/profile`, formData);
    }

    getAll() {
        return this.http.get(`${environment.apiUrl}/users`);
    }

    register(user) {
        return this.http.post(`${environment.apiUrl}/users/register`, user);
    }

    delete(id) {
        return this.http.delete(`${environment.apiUrl}/users/${id}`);
    }
}