﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FornecedoresService {
  constructor(private http: HttpClient) { }

  getFornecedor(id_loja: number, matricula?: string, cpf?: string): Observable<any> {
    const params = new HttpParams().set('matricula', matricula || '').set('cpf', cpf || '');
    return this.http.get(`${environment.apiUrl}/fornecedores/${id_loja}`, { params });
  }

  getFornecedores(id_loja: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/fornecedores/${id_loja}`);
  }

  recibosCompra(formData: FormData): Observable<any> {
    return this.http.post(`${environment.apiUrl}/reciboscompra`, formData);
  }

  recibosSaque(formData: FormData): Observable<any> {
    return this.http.post(`${environment.apiUrl}/recibossaque`, formData);
  }

  extrato(id: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/extrato/${id}`);
  }

  extratoEmail(formData: FormData): Observable<any> {
    return this.http.post(`${environment.apiUrl}/extrato/email`, formData);
  }

  reciboEmail(formData: FormData): Observable<any> {
    return this.http.post(`${environment.apiUrl}/recibos/email`, formData);
  }
}
