import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FornecedoresService } from 'src/app/_services/fornecedores.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-recibosaque',
  templateUrl: './recibosaque.component.html',
  styleUrls: ['./recibosaque.component.less']
})
export class RecibosaqueComponent implements OnInit {
  currentUserTCGED: User;
  points = [];
  forn = [];
  signatureImage;

  formSaque = new FormGroup({
    valorD: new FormControl(0),
    valorC: new FormControl(0),
    valorT: new FormControl(0)
  });

  recibo = false;
  assinado = false;
  finalizado = false;
  loading = false;
  message: any = {};
  id: string;
  pdf;

  showImage(data) {
    this.signatureImage = data;
    this.assinado = true;
  }

  constructor(
    private router: ActivatedRoute,
    private fornService: FornecedoresService,
    private authenticationService: AuthenticationService
  ) {
    this.currentUserTCGED = this.authenticationService.currentUserTCGEDValue;
  }

  submitForm() {
    this.message = {};
    if (this.formSaque.value.valorD + this.formSaque.value.valorC + this.formSaque.value.valorT > (this.forn['saldo_liberado'] + this.forn['saldo_bloqueado'])) {
      this.message = {'erro': true, 'msg': 'Valor não pode ser maior que o saldo disponível para saque.'};
      return;
    }
    this.recibo = true;
  }

  validForm() {
    if(this.formSaque.get('valorD').value + this.formSaque.get('valorC').value + this.formSaque.get('valorT').value == 0) {
      return false;
    }
    if(this.formSaque.get('valorD').value == null || this.formSaque.get('valorC').value == null || this.formSaque.get('valorT').value == null) {
      return false;
    }
    return true;
  }

  submitAssinatura() {
    this.loading = true;
    this.message = {};
    const formData = new FormData();
    formData.append('user', this.currentUserTCGED.id.toString());
    formData.append('store', this.currentUserTCGED.loja_id.toString());
    formData.append('provider', this.forn['id']);
    formData.append('money', this.formSaque.get('valorD').value);
    formData.append('bank_check', this.formSaque.get('valorC').value);
    formData.append('bank_transfer', this.formSaque.get('valorT').value);
    formData.append('signature', this.signatureImage);
    this.fornService.recibosSaque(formData).subscribe(
      (res) => {
        console.log(res);
        this.message = {'erro': false, 'msg': res.message};
        this.getFornecedor();
        this.pdf = res.msg.img_pdf
        this.loading = false;
        this.finalizado = true;
        this.formSaque.reset();
      },
      (err) => {
        console.log(err);
        this.message = {'erro': true, 'msg': err.error.message};
        this.loading = false;
      }
    )
  }

  getFornecedor() {
    this.fornService.getFornecedor(this.currentUserTCGED.loja_id, this.id)
      .subscribe(f => {
        this.forn = f.data[0];
      },
        (err) => this.message = {'erro': true, 'msg': err.error.message});
  }

  ngOnInit() {
    this.id = this.router.snapshot.params.id;
    this.getFornecedor();
  }

}
