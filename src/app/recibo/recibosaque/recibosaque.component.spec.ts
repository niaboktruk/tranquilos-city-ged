import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosaqueComponent } from './recibosaque.component';

describe('RecibosaqueComponent', () => {
  let component: RecibosaqueComponent;
  let fixture: ComponentFixture<RecibosaqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosaqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosaqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
