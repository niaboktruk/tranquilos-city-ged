import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FornecedoresService } from 'src/app/_services/fornecedores.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/_services';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-recibocompra',
  templateUrl: './recibocompra.component.html',
  styleUrls: ['./recibocompra.component.less']
})
export class RecibocompraComponent implements OnInit {
  currentUserTCGED: User;
  points = [];
  forn = [];
  signatureImage;

  formCompraProdutos = new FormGroup({
    valor: new FormControl('', Validators.required),
    nota: new FormControl('', Validators.required),
  });

  recibo = false;
  assinado = false;
  loading = false;
  finalizado = false;
  message: any = {};
  pdf;
  id: string;

  showImage(data) {
    this.signatureImage = data;
    this.assinado = true;
  }

  constructor(
    private router: ActivatedRoute,
    private fornService: FornecedoresService,
    private authenticationService: AuthenticationService
  ) {
    this.currentUserTCGED = this.authenticationService.currentUserTCGEDValue;
  }

  submitForm() {
    this.message = {};
    if (this.formCompraProdutos.value.valor > (this.forn['saldo_liberado'] + this.forn['saldo_bloqueado'])) {
      this.message = { 'erro': true, 'msg': 'Valor não pode ser maior que o saldo para uso.' };
      return;
    }
    this.recibo = true;
  }

  submitAssinatura() {
    this.loading = true;
    this.message = {};
    const formData = new FormData();
    formData.append('user', this.currentUserTCGED.id.toString());
    formData.append('store', this.currentUserTCGED.loja_id.toString());
    formData.append('provider', this.forn['id']);
    formData.append('invoice', this.formCompraProdutos.get('nota').value);
    formData.append('value', this.formCompraProdutos.get('valor').value);
    formData.append('signature', this.signatureImage);
    this.fornService.recibosCompra(formData).subscribe(
      (res) => {
        console.log(res);
        this.message = { 'erro': false, 'msg': res.message };
        this.getFornecedor();
        this.pdf = res.msg.img_pdf;
        this.loading = false;
        this.formCompraProdutos.reset();
        this.finalizado = true;
      },
      (err) => {
        console.log(err);
        this.message = { 'erro': true, 'msg': err.error.message };
        this.loading = false;
      })
  }

  getFornecedor() {
    this.fornService.getFornecedor(this.currentUserTCGED.loja_id, this.id)
      .subscribe(f => {
        this.forn = f.data[0];
      },
        (err) => this.message = { 'erro': true, 'msg': err.error.message });
  }

  ngOnInit() {
    this.id = this.router.snapshot.params.id;
    this.getFornecedor();
  }

}
