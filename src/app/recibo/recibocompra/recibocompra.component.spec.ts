import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibocompraComponent } from './recibocompra.component';

describe('RecibocompraComponent', () => {
  let component: RecibocompraComponent;
  let fixture: ComponentFixture<RecibocompraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibocompraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibocompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
