import { User } from 'src/app/_models/user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FornecedoresService } from 'src/app/_services/fornecedores.service';
import { AuthenticationService } from 'src/app/_services';
import { Location } from '@angular/common';

@Component({
  selector: 'app-extrato',
  templateUrl: './extrato.component.html',
  styleUrls: ['./extrato.component.less']
})
export class ExtratoComponent implements OnInit {

  currentUserTCGED: User;
  id;
  forn = [];
  message: any = {};

  constructor(
    private router: ActivatedRoute,
    private fornService: FornecedoresService,
    private authenticationService: AuthenticationService,
    public _location: Location
  ) {
    this.currentUserTCGED = this.authenticationService.currentUserTCGEDValue;
    console.log(this.currentUserTCGED);
  }

  enviarExtrato() {
    this.message = {};
    const formData = new FormData();
    formData.append('id', this.forn[0].fornecedor_id);
    this.fornService.extratoEmail(formData).subscribe(
      (res) => {
        this.message = { 'erro': false, 'msg': res.message };
      },
      (err) => {
        console.log(err);
        this.message = { 'erro': true, 'msg': err.error.message };
      }
    )
  }

  enviarRecibo(id) {
    this.message = {};
    const formData = new FormData();
    formData.append('id', this.forn[0].fornecedor_id);
    formData.append('recibo_id', id);
    this.fornService.reciboEmail(formData).subscribe(
      (res) => {
        this.message = { 'erro': false, 'msg': res.message };
      },
      (err) => {
        console.log(err);
        this.message = { 'erro': true, 'msg': err.error.message };
      }
    )
  }

  ngOnInit() {
    this.id = this.router.snapshot.params.id;
    this.fornService.extrato(this.id)
      .subscribe(f => {
        this.forn = f.data;
      },
        (err) => this.message = { 'erro': true, 'msg': err.error.message });
  }

}
