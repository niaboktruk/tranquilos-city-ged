import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { FornecedoresService } from 'src/app/_services/fornecedores.service'
import { User } from 'src/app/_models/user';
import { AuthenticationService, AlertService } from 'src/app/_services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pesquisaforn',
  templateUrl: './pesquisaforn.component.html',
  styleUrls: ['./pesquisaforn.component.less']
})
export class PesquisafornComponent implements OnInit {

  formForn = new FormGroup({
    codigoCliente: new FormControl(''),
    cpf: new FormControl(''),
  });

  currentUserTCGED: User;
  rota;
  titulo = '';

  tabela = false;
  forn = [];

  constructor(
    private route: ActivatedRoute,
    private fornService: FornecedoresService,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) {
    this.currentUserTCGED = this.authenticationService.currentUserTCGEDValue;
    this.alertService.clear();
    this.route.params.subscribe(param => {
      this.rota = param['rota'];
      switch (this.rota) {
        case 'extrato':
          this.titulo = 'Extrato de Movimentação';
          break;
        case 'recibocompra':
          this.titulo = 'Compra de Produtos';
          break;
        case 'recibosaque':
          this.titulo = 'Solicitação de saque';
          break;
        case 'contrato':
          this.titulo = 'Contratos';
      }
    })
  }

  submitForm() {
    this.tabela = true;
    this.alertService.clear();
    this.forn = [];

    this.fornService.getFornecedor(this.currentUserTCGED.loja_id ? this.currentUserTCGED.loja_id : this.currentUserTCGED.empresa_master_id, this.formForn.value.codigoCliente, this.formForn.value.cpf)
      .subscribe(f => {
        this.forn = f.data;
      },
        (err) => this.alertService.error(err.error.message));
  }

  ngOnInit() {
  }

}
