import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from '../_services';
import { User } from '../_models/user';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {

  message;
  messageOk;
  currentUserTCGED: User;
  usuario = [];
  points = [];
  signatureImage;
  assinado = false;
  fotoUpload: File = null;

  formPerfil = new FormGroup({
    email: new FormControl(''),
    senha: new FormControl(''),
    senha2: new FormControl(''),
    foto: new FormControl('')
  });

  constructor(
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {
    this.currentUserTCGED = this.authenticationService.currentUserTCGEDValue;
  }

  showImage(data) {
    this.signatureImage = data;
    this.assinado = true;
  }

  fileInput(files: FileList) {
    this.fotoUpload = files.item(0);
  }

  submitForm() {
    this.message = '';
    this.messageOk = '';
    const formData = new FormData();
    formData.append('user', this.currentUserTCGED.id.toString());
    formData.append('email', this.formPerfil.get('email').value);
    formData.append('password', this.formPerfil.get('senha').value);
    formData.append('password_confirmation', this.formPerfil.get('senha2').value);
    if (this.fotoUpload) formData.append('uploadProfileImg', this.fotoUpload, this.fotoUpload.name);
    formData.append('uploadProfileSign', this.signatureImage || '');
    this.userService.salvarPerfil(formData).subscribe(
      (res) => {
        console.log(res);
        this.messageOk = res['message'];
      },
      (err) => {
        console.log(err);
        this.message = err.error.message;
      })
  }

  ngOnInit() {
    this.userService.perfil(this.currentUserTCGED.loja_id)
      .subscribe(u => {
        this.usuario = u['msg'];
        this.formPerfil.get("email").setValue(this.usuario['email']);
      },
        (err) => this.message = err.error.message);
  }

}
