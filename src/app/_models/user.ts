﻿export class User {
    id: number;
    apelido: string;
    password: string;
    nome: string;
    sobrenome: string;
    loja_id: number;
    empresa_master_id: number;
    token: string;
    fields: [];
}
